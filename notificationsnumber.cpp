#include "notificationsnumber.h"

NotificationsNumber::NotificationsNumber()
{

}

int NotificationsNumber::unreadConversations() const
{
    return mUnreadConversations;
}

void NotificationsNumber::setUnreadConversations(int unreadConversations)
{
    mUnreadConversations = unreadConversations;
}

int NotificationsNumber::newNotifs() const
{
    return mNewNotifs;
}

void NotificationsNumber::setNewNotifs(int newNotifs)
{
    mNewNotifs = newNotifs;
}

int NotificationsNumber::sum() const
{
    return mUnreadConversations + mNewNotifs;
}
