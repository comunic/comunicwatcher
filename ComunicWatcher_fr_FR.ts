<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="en">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="14"/>
        <source>About ComunicWatcher</source>
        <translation>A propos de ComunicWatcher</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="26"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ComunicWatcher&lt;/span&gt;&lt;/p&gt;&lt;p&gt;This software is part of Comunic, a free &amp;amp; OpenSource social Network.&lt;/p&gt;&lt;p&gt;This application was built by Pierre Hubert on 2020.&lt;/p&gt;&lt;p&gt;It is licensed under the MIT License.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://communiquons.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;https://communiquons.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ComunicWatcher&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Ce logiciel fait partie de Comunic, un r&amp;eacute;seau social libre et OpenSource.&lt;/p&gt;&lt;p&gt;Cette application a &amp;eacute;t&amp;eacute; d&amp;eacute;velopp&amp;eacute;e par Pierre Hubert en 2020.&lt;/p&gt;&lt;p&gt;Son code source diffusé sous licence MIT.&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://communiquons.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;https://communiquons.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="35"/>
        <source>User ID</source>
        <translation>Identifiant utilisateur</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="42"/>
        <location filename="aboutdialog.ui" line="56"/>
        <source>Loading</source>
        <translation>Chargement en cours</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="49"/>
        <source>User name</source>
        <translation>Nom de l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="28"/>
        <location filename="aboutdialog.cpp" line="38"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="28"/>
        <source>Could not get user information!</source>
        <translation>Erreur lors de la récupération de vos informations !</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="38"/>
        <source>Could not get your id!</source>
        <translation>Erreur lors de la récupération de votre identifiant utilisateur !</translation>
    </message>
</context>
<context>
    <name>LoginSuccessfulDialog</name>
    <message>
        <location filename="loginsuccessfuldialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="loginsuccessfuldialog.ui" line="28"/>
        <source>Login successful !</source>
        <translation>Connexion effectuée avec succès !</translation>
    </message>
    <message>
        <location filename="loginsuccessfuldialog.ui" line="51"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="loginsuccessfuldialog.ui" line="64"/>
        <source>This application will now be visible from the system tray and will send you notifications when there is new activity on Comunic....</source>
        <translation>Cette application sera maintenant visible dans la barre des tâches et vous enverra des notifications lorsqu&apos;il y a de nouvelles activités sur Comunic...</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <location filename="loginwindow.ui" line="17"/>
        <source>LoginWindow</source>
        <translation>Fenêtre de connexion</translation>
    </message>
    <message>
        <location filename="loginwindow.ui" line="80"/>
        <source>Login to Comunic</source>
        <translation>Connexion à Comunic</translation>
    </message>
    <message>
        <location filename="loginwindow.ui" line="103"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="loginwindow.ui" line="142"/>
        <source>Email address</source>
        <translation>Adresse mail</translation>
    </message>
    <message>
        <location filename="loginwindow.ui" line="152"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="loginwindow.ui" line="166"/>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="loginwindow.cpp" line="46"/>
        <location filename="loginwindow.cpp" line="51"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="loginwindow.cpp" line="46"/>
        <source>Please specify an email address!</source>
        <translation>Veuillez spécifier une adresse mail !</translation>
    </message>
    <message>
        <location filename="loginwindow.cpp" line="51"/>
        <source>Please specify your password!</source>
        <translation>Veuillez spécifier votre mot de passe !</translation>
    </message>
    <message>
        <location filename="loginwindow.cpp" line="81"/>
        <source>Invalid credentials!</source>
        <translation>Identifiants de connexion incorrects !</translation>
    </message>
    <message>
        <location filename="loginwindow.cpp" line="85"/>
        <source>Too many login attempt, please try again later!</source>
        <translation>Trop de tentatives de connexion, veuillez réessayer ultérieurement !</translation>
    </message>
    <message>
        <location filename="loginwindow.cpp" line="89"/>
        <source>An error occured while trying to sign you in!</source>
        <translation>Une erreur a survenue lors de votre tentative de connexion !</translation>
    </message>
    <message>
        <location filename="loginwindow.cpp" line="94"/>
        <source>Login failed</source>
        <translation>Echec de la connexion</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="19"/>
        <source>Unsupported system</source>
        <translation>Système non pris en charge</translation>
    </message>
    <message>
        <location filename="main.cpp" line="19"/>
        <source>Unfortunately, your system is not supported by this application...</source>
        <translation>Malheureusement, votre système d&apos;exploitation nest pas pris en charge par cette application...</translation>
    </message>
</context>
<context>
    <name>RefreshService</name>
    <message>
        <location filename="refreshservice.cpp" line="77"/>
        <source>Sign out</source>
        <translation>Déconnexion</translation>
    </message>
    <message>
        <location filename="refreshservice.cpp" line="77"/>
        <source>Do you really want to disconnect ComunicWatcher from your Comunic account?</source>
        <translation>Voulez-vous vraiment déconnecter ComunicWatcher de votre compte Comunic ?</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="trayicon.cpp" line="12"/>
        <location filename="trayicon.cpp" line="15"/>
        <source>Loading...</source>
        <translation>Chargement en cours...</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="18"/>
        <source>Open Comunic</source>
        <translation>Ouvrir Comunic</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="23"/>
        <source>Other actions</source>
        <translation>Autres actions</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="25"/>
        <source>About this application</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="28"/>
        <source>Sign out</source>
        <translation>Déconnexion</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="33"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="58"/>
        <source>%1 notification</source>
        <translation>%1 notification</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="59"/>
        <source>%1 notifications</source>
        <translation>%1 notifications</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="64"/>
        <source>%1 unread conversation</source>
        <translation>%1 conversation non lue</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="65"/>
        <location filename="trayicon.cpp" line="92"/>
        <source>%1 unread conversations</source>
        <translation>%1 conversations non lues</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="80"/>
        <source>1 new notification</source>
        <translation>1 nouvelle notification</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="82"/>
        <source>%1 new notifications</source>
        <translation>%1 nouvelles notifications</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="87"/>
        <source> and </source>
        <translation> et </translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="90"/>
        <source>1 unread conversation</source>
        <translation>1 conversation non lue</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="95"/>
        <source>You have %1.</source>
        <translation>Vous avez %1</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="97"/>
        <source>Comunic</source>
        <translation>Comunic</translation>
    </message>
</context>
</TS>
