/**
 * WebSocket client
 *
 * @author Pierre Hubert
 */

#pragma once

#include "apiresponse.h"

#include <QObject>
#include <QWebSocket>

class WsClient : public QObject
{
    Q_OBJECT
public:
    explicit WsClient(QObject *parent = nullptr);


public slots:
    void startConnect();

signals:
    void connected();
    void newNumberNotifs(int num);
    void newNumberConvs(int num);

private slots:
    void onConnectionError();
    void getTokenCallBack(APIResponse res);
    void onConnected();
    void onMessage(const QString &msg);


private:
    // Class members
    QWebSocket mWebSocket;
};

