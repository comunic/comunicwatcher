/**
 * Simple API request
 *
 * @author Pierre Hubert
 */

#pragma once

#include "apiresponse.h"

#include <QObject>
#include <QMap>
#include <QNetworkAccessManager>

class APIRequest : public QObject
{
    Q_OBJECT
public:
    explicit APIRequest(const QString &uri, QObject *parent = nullptr);

    /**
     * Add a new parameter to this request
     */
    void addString(const QString &name, const QString &value);
    void addInt(const QString &name, int value);

    /**
     * Execute the request
     */
    void exec();

signals:
    void done(APIResponse response);

private slots:
    void onResponse();

private:
    QString mURI;
    QMap<QString, QString> mArgs;
    static QNetworkAccessManager mNetworkManager;
    QNetworkReply *mReply;
};

