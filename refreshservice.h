/**
 * Refresh service
 *
 * @author Pierre Hubert
 */

#pragma once

#include "notificationshelper.h"
#include "trayicon.h"
#include "wsclient.h"

#include <QObject>

class RefreshService : public QObject
{
    Q_OBJECT

public:
    static void startService();
    static void stopService();

    static void signOutUser();

private slots:
    void connectedToWebSocket();
    void onNewNumberNotifications(NotificationsNumber number);
    void onNewNeumberUnreadNotifs(int num);
    void onNewNeumberUnreadConvs(int num);
    void openComunic();
    void openAboutDialog();
    void confirmSignOut();

private:
    RefreshService();
    ~RefreshService();

    // Class members
    static RefreshService *svc;
    WsClient mWsClient;
    NotificationsHelper mNotifsHelper;
    NotificationsNumber mNumbers;
    TrayIcon mTrayIcon;
};
