#include "accounthelper.h"
#include "apirequest.h"
#include "config.h"
#include "loginwindow.h"
#include "refreshservice.h"

#include <QHttpPart>
#include <QNetworkReply>

QNetworkAccessManager APIRequest::mNetworkManager;

APIRequest::APIRequest(const QString &uri, QObject *parent) : QObject(parent), mURI(uri)
{
    addString("serviceName", API_SERVICE_NAME);
    addString("serviceToken", API_SERVICE_TOKEN);

    addString("incognito", "true");

    if(AccountHelper::SignedIn()) {
        addString("userToken1", AccountHelper::GetLoginToken());
        addString("userToken2", "dummy_data");
    }
}

void APIRequest::addString(const QString &name, const QString &value)
{
    mArgs[name] = value;
}

void APIRequest::addInt(const QString &name, int value)
{
    mArgs[name] = QString::number(value);
}

void APIRequest::exec()
{
    QUrl url;
    url.setScheme(API_SERVER_SCHEME);
    url.setHost(API_HOST);
    url.setPath(API_BASE_PATH + mURI);

    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    QString query = "";
    for(QString val : mArgs.keys()) {
        if(!query.isEmpty())
            query += "&";

        query += QUrl::toPercentEncoding(val) + "=" + QUrl::toPercentEncoding(mArgs[val]);
    }


    // Execute request
    mReply = mNetworkManager.post(req, query.toUtf8());
    mReply->setParent(this);
    connect(mReply, &QNetworkReply::finished, this, &APIRequest::onResponse);
}

void APIRequest::onResponse()
{
    int code = mReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QByteArray content = mReply->readAll();

    deleteLater();
    qDebug("Request: %s - %d - %s", mURI.toStdString().c_str(), code, content.toStdString().c_str());


    // Check if login token was destroyed
    if(code == 412) {
        RefreshService::signOutUser();
        return;
    }


    emit done(APIResponse(code, content));

}
