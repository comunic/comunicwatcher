#include "loginsuccessfuldialog.h"
#include "ui_loginsuccessfuldialog.h"

LoginSuccessfulDialog::LoginSuccessfulDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginSuccessfulDialog)
{
    ui->setupUi(this);
    setWindowFlag(Qt::FramelessWindowHint);
}

LoginSuccessfulDialog::~LoginSuccessfulDialog()
{
    delete ui;
}
