#pragma once

#include <QDialog>

namespace Ui {
class LoginSuccessfulDialog;
}

class LoginSuccessfulDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginSuccessfulDialog(QWidget *parent = nullptr);
    ~LoginSuccessfulDialog();

private:
    Ui::LoginSuccessfulDialog *ui;
};

