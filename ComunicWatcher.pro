QT       += core gui widgets network websockets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    aboutdialog.cpp \
    accounthelper.cpp \
    apirequest.cpp \
    apiresponse.cpp \
    loginsuccessfuldialog.cpp \
    main.cpp \
    loginwindow.cpp \
    notificationshelper.cpp \
    notificationsnumber.cpp \
    refreshservice.cpp \
    trayicon.cpp \
    user.cpp \
    userhelper.cpp \
    wsclient.cpp

HEADERS += \
    aboutdialog.h \
    accounthelper.h \
    apirequest.h \
    apiresponse.h \
    config.h \
    loginsuccessfuldialog.h \
    loginwindow.h \
    notificationshelper.h \
    notificationsnumber.h \
    refreshservice.h \
    trayicon.h \
    user.h \
    userhelper.h \
    wsclient.h

FORMS += \
    aboutdialog.ui \
    loginsuccessfuldialog.ui \
    loginwindow.ui

TRANSLATIONS += \
    ComunicWatcher_fr_FR.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    ressources.qrc

RC_ICONS = app_icon.ico
