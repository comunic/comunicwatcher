#pragma once

#include "user.h"

#include <QObject>

class UserHelper : public QObject
{
    Q_OBJECT
public:
    explicit UserHelper(QObject *parent = nullptr);

    /**
     * Get information about a user
     *
     * @param userID Target user ID
     */
    void getUserInfo(int userID);

signals:
    void onGotUserInfo(const User &u);

};

