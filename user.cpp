#include "user.h"

User::User()
{

}

bool User::isValid() const
{
    return mId > 0;
}

int User::id() const
{
    return mId;
}

void User::setId(int id)
{
    mId = id;
}

QString User::firstName() const
{
    return mFirstName;
}

void User::setFirstName(const QString &firstName)
{
    mFirstName = firstName;
}

QString User::lastName() const
{
    return mLastName;
}

void User::setLastName(const QString &lastName)
{
    mLastName = lastName;
}

QString User::fullName() const
{
    return mFirstName + " " + mLastName;
}
