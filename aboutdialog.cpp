#include <QMessageBox>

#include "accounthelper.h"
#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    AccountHelper::GetUserID([this](int id) {
        this->onGotUserId(id);
    });

    connect(&mUserHelper, &UserHelper::onGotUserInfo, this, &AboutDialog::getUserCallback);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::getUserCallback(const User &u)
{
    if(!u.isValid()) {
        QMessageBox::warning(this, tr("Error"), tr("Could not get user information!"));
        return;
    }

    this->ui->user_name->setText(u.fullName());
}

void AboutDialog::onGotUserId(int userID)
{
    if(userID < 1) {
        QMessageBox::warning(this, tr("Error"), tr("Could not get your id!"));
        return;
    }

    this->ui->user_id->setText(QString::number(userID));

    mUserHelper.getUserInfo(userID);
}
