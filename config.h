/**
 * Configuration
 *
 * @author Pierre Hubert
 */

#pragma once


/**
 * Release configuration
 */
#ifdef QT_NO_DEBUG
#define API_SERVER_SCHEME "https"
#define API_HOST "api.communiquons.org"
#define API_BASE_PATH "/"
#define API_SERVICE_NAME "ComunicWatcher"
#define API_SERVICE_TOKEN "yUrFWm9qQ7"
#define COMUNIC_URL "https://communiquons.org/"
#endif


/**
 * Debug configuration
 */
#ifndef API_HOST
#define API_SERVER_SCHEME "https"
#define API_HOST "devweb.local"
#define API_BASE_PATH "/comunic/api-v2/"
#define API_SERVICE_NAME "ComunicWatcher"
#define API_SERVICE_TOKEN "ComunicWatcher"
#define COMUNIC_URL "https://devweb.local/comunic/v2/"
#endif
