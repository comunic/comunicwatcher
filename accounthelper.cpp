#include "accounthelper.h"
#include "apirequest.h"

#include <QSettings>

#define LOGIN_TOKEN_VALUE "login_token"

void AccountHelper::LoginUser(const QString &email, const QString &password, const vLoginCallback cb)
{
    auto req = new APIRequest("account/login");
    req->addString("userMail", email);
    req->addString("userPassword", password);
    req->exec();
    QObject::connect(req, &APIRequest::done, [=] (APIResponse res) {

        QString token;

        switch(res.getCode()) {

        case 200:
            // Save login token
            token = res.getObject().value("tokens").toObject().value("token1").toString();
            QSettings().setValue(LOGIN_TOKEN_VALUE, token);

            cb(LoginResult::SUCCESS);
            break;

        case 401:
            cb(LoginResult::BAD_PASSWORD);
            break;

        case 429:
            cb(LoginResult::TOO_MANY_REQUEST);
            break;

        default:
            cb(LoginResult::ERROR);
            break;
        }

    });
}

bool AccountHelper::SignedIn()
{
    return QSettings().value(LOGIN_TOKEN_VALUE).isValid();
}

QString AccountHelper::GetLoginToken()
{
    return QSettings().value(LOGIN_TOKEN_VALUE).toString();
}

void AccountHelper::RemoveLoginToken()
{
    QSettings().remove(LOGIN_TOKEN_VALUE);
}

void AccountHelper::GetUserID(const vGetUserIDCallback cb)
{
    auto req = new APIRequest("account/id");
    req->exec();
    QObject::connect(req, &APIRequest::done, [=] (APIResponse res) {
        if(res.isError())
            cb(-1);
        else
            cb(res.getObject().value("userID").toInt(-1));
    });

}
