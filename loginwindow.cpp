#include "accounthelper.h"
#include "apirequest.h"
#include "loginsuccessfuldialog.h"
#include "loginwindow.h"
#include "refreshservice.h"
#include "ui_loginwindow.h"

#include <QMessageBox>

LoginWindow::LoginWindow(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::LoginWindow)
{
    ui->setupUi(this);
    setWindowFlag(Qt::FramelessWindowHint);
    setLoading(false);
}

LoginWindow::~LoginWindow()
{
    delete ui;
}


void LoginWindow::on_closeButton_clicked()
{
    this->close();
}


void LoginWindow::mousePressEvent(QMouseEvent *evt)
{
    mOldPos = evt->globalPos();
}

void LoginWindow::mouseMoveEvent(QMouseEvent *evt)
{
    const QPoint delta = evt->globalPos() - mOldPos;
    move(x()+delta.x(), y()+delta.y());
    mOldPos = evt->globalPos();
}

void LoginWindow::submitForm()
{
    if(ui->emailEdit->text().isEmpty()) {
        QMessageBox::warning(this, tr("Error"), tr("Please specify an email address!"));
        return;
    }

    if(ui->passwordEdit->text().isEmpty()) {
        QMessageBox::warning(this, tr("Error"), tr("Please specify your password!"));
        return;
    }

    setLoading(true);

    AccountHelper::LoginUser(
                ui->emailEdit->text(),
                ui->passwordEdit->text(),
                [&](LoginResult res) {
        this->onResponse(res);
    });
}

void LoginWindow::onResponse(LoginResult res)
{
    QString msg;

    switch(res) {

    case SUCCESS:
        LoginSuccessfulDialog(this).exec();

        RefreshService::startService();

        close();
        deleteLater();
        return;

    case BAD_PASSWORD:
        msg = tr("Invalid credentials!");
        break;

    case TOO_MANY_REQUEST:
        msg = tr("Too many login attempt, please try again later!");
        break;

    default:
        msg = tr("An error occured while trying to sign you in!");
        break;
    }

    setLoading(false);
    QMessageBox::warning(this, tr("Login failed"), msg);
}

void LoginWindow::on_submitButton_clicked()
{
    submitForm();
}

void LoginWindow::setLoading(bool loading)
{
    ui->loginProgress->setVisible(loading);
    ui->loginFormContainer->setVisible(!loading);
}
