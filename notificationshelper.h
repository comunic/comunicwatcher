/**
 * Notifications helper
 *
 * @author Pierre Hubert
 */

#pragma once

#include <QObject>

#include "notificationsnumber.h"
#include "apiresponse.h"

class NotificationsHelper : public QObject
{
    Q_OBJECT
public:
    explicit NotificationsHelper(QObject *parent = nullptr);

    /**
     * Count the number of new notifications
     */
    void getNewNotificationsNumbers();

signals:
    void onNewNumber(NotificationsNumber notifs);



private slots:
    void getNotificationsNumberCallback(APIResponse response);
};

