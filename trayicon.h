/**
 * Tray icon
 *
 * @author Pierre Hubert
 */

#pragma once

#include <QAction>
#include <QObject>
#include <QSystemTrayIcon>

#include "notificationsnumber.h"

class TrayIcon : public QObject
{
    Q_OBJECT
public:
    explicit TrayIcon(QObject *parent = nullptr);
    ~TrayIcon();

public slots:
    void onNewNumber(const NotificationsNumber &number);

signals:
    void onOpenComunic();
    void showAboutDialog();
    void askForSignOut();

private slots:
    void onQuit();

private:

    /**
     * Show a notification
     *
     * @param n The number of unread conversations
     */
    void showNotification(const NotificationsNumber &n);

    /**
     * Refresh the icon shown in the taskbar
     */
    void refreshIcon();

    // Class members
    QMenu *mMenu;
    QAction *mUnreadNotifications;
    QAction *mUnreadConversations;
    QSystemTrayIcon mTrayIcon;
    int mOldNumber = 0;
};

