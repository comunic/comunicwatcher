/**
 * Notifications number
 *
 * @author Pierre Hubert
 */

#pragma once


class NotificationsNumber
{
public:
    NotificationsNumber();

    int unreadConversations() const;
    void setUnreadConversations(int unreadConversations);

    int newNotifs() const;
    void setNewNotifs(int newNotifs);

    int sum() const;

private:
    int mNewNotifs;
    int mUnreadConversations;
};

