#include "apirequest.h"
#include "userhelper.h"

UserHelper::UserHelper(QObject *parent) : QObject(parent)
{

}

void UserHelper::getUserInfo(int userID)
{
    auto req = new APIRequest("user/getInfo");
    req->addInt("userID", userID);
    req->exec();
    connect(req, &APIRequest::done, [=](APIResponse res) {

        if(res.isError()) {
            emit onGotUserInfo(User());
            return;
        }

        User u;
        auto obj = res.getObject();
        u.setId(obj.value("userID").toInt());
        u.setFirstName(obj.value("firstName").toString());
        u.setLastName(obj.value("lastName").toString());
        emit onGotUserInfo(u);
    });
}
