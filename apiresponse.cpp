#include <QJsonDocument>

#include "apiresponse.h"

APIResponse::APIResponse(int code, QByteArray content) :
    mCode(code), mContent(content)
{

}

int APIResponse::getCode() const
{
    return mCode;
}

QByteArray APIResponse::getContent() const
{
    return mContent;
}



bool APIResponse::isError()
{
    return mCode != 200;
}

QJsonObject APIResponse::getObject() const
{
    return QJsonDocument::fromJson(mContent).object();
}
