#include <QApplication>
#include <QMessageBox>
#include <QSystemTrayIcon>

#include "loginwindow.h"
#include "refreshservice.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Initialize configuration
    QCoreApplication::setOrganizationName("Communiquons");
    QCoreApplication::setOrganizationDomain("communiquons.org");
    QCoreApplication::setApplicationName("ComunicWatcher");

    // Check if the system is compatible
    if(!QSystemTrayIcon::isSystemTrayAvailable()) {
        QMessageBox::warning(nullptr, QObject::tr("Unsupported system"), QObject::tr("Unfortunately, your system is not supported by this application..."));
        return -1;
    }

    // Useful information in case of upgrade
    qDebug() << "Required version of OpenSSL: " << QSslSocket::sslLibraryBuildVersionString();

    if(!AccountHelper::SignedIn()) {
        (new LoginWindow())->show();
    }

    else
        RefreshService::startService();

    return a.exec();
}
