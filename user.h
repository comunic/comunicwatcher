/**
 * User information
 *
 * This class contains information about a user
 *
 * @author Pierre Hubert
 */

#pragma once

#include <QString>

class User
{
public:
    User();

    bool isValid() const;

    int id() const;
    void setId(int id);

    QString firstName() const;
    void setFirstName(const QString &firstName);

    QString lastName() const;
    void setLastName(const QString &lastName);

    QString fullName() const;

private:
    int mId;
    QString mFirstName;
    QString mLastName;
};

