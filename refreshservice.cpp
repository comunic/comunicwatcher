#include <QGuiApplication>
#include <QMessageBox>
#include <QDesktopServices>

#include "accounthelper.h"
#include "loginwindow.h"
#include "refreshservice.h"
#include "config.h"
#include "aboutdialog.h"

RefreshService *RefreshService::svc = nullptr;


void RefreshService::startService()
{
    if(svc != nullptr)
        return;

    QGuiApplication::setQuitOnLastWindowClosed(false);

    svc = new RefreshService();
}

void RefreshService::stopService()
{
    if(svc != nullptr)
        svc->deleteLater();

    QGuiApplication::setQuitOnLastWindowClosed(true);

    svc = nullptr;
}

void RefreshService::signOutUser()
{
    AccountHelper::RemoveLoginToken();
    RefreshService::stopService();
    (new LoginWindow())->show();
}

void RefreshService::connectedToWebSocket()
{
    mNotifsHelper.getNewNotificationsNumbers();
}

void RefreshService::onNewNumberNotifications(NotificationsNumber numbers)
{
    mNumbers = numbers;

    mTrayIcon.onNewNumber(numbers);
}

void RefreshService::onNewNeumberUnreadNotifs(int num)
{
    mNumbers.setNewNotifs(num);
    mTrayIcon.onNewNumber(mNumbers);
}

void RefreshService::onNewNeumberUnreadConvs(int num)
{
    mNumbers.setUnreadConversations(num);
    mTrayIcon.onNewNumber(mNumbers);
}

void RefreshService::openComunic()
{
    QDesktopServices::openUrl(QUrl(COMUNIC_URL));
}

void RefreshService::openAboutDialog()
{
    AboutDialog().exec();
}

void RefreshService::confirmSignOut()
{
    if(QMessageBox::question(nullptr, tr("Sign out"), tr("Do you really want to disconnect ComunicWatcher from your Comunic account?")) != QMessageBox::Yes)
        return;

    signOutUser();
}

RefreshService::RefreshService()
{
    qDebug("Start refresh service");

    connect(&mWsClient, &WsClient::connected, this, &RefreshService::connectedToWebSocket);
    connect(&mNotifsHelper, &NotificationsHelper::onNewNumber, this, &RefreshService::onNewNumberNotifications);
    connect(&mWsClient, &WsClient::newNumberNotifs, this, &RefreshService::onNewNeumberUnreadNotifs);
    connect(&mWsClient, &WsClient::newNumberConvs, this, &RefreshService::onNewNeumberUnreadConvs);
    connect(&mTrayIcon, &TrayIcon::askForSignOut, this, &RefreshService::confirmSignOut);
    connect(&mTrayIcon, &TrayIcon::showAboutDialog, this, &RefreshService::openAboutDialog);
    connect(&mTrayIcon, &TrayIcon::onOpenComunic, this, &RefreshService::openComunic);
}

RefreshService::~RefreshService()
{
    qDebug("Stop refresh service");
}
