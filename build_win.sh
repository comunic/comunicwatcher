#export QTDIR=/c/Qt/Qt5.12.8/5.12.8/mingw73_64
#export PATH=$PATH:/c/Qt/Qt5.12.8/Tools/mingw730_32/bin/
#export PATH=$PATH:$QTDIR/bin/
#export PATH=$PATH:/c/Qt/Qt5.12.8/Tools/mingw730_32/bin/

#qmake -config release  -spec win32-g++ "CONFIG+=qtquickcompiler"
#mingw32-make.exe qmake_all
#mingw32-make.exe -f Makefile.Release


#bash

export RELEASE_PATH=~/Documents/projects/build-ComunicWatcher-Desktop_Qt_5_12_8_MinGW_64_bit-Release/release/
export MINGW_BIN=/c/Qt/Qt5.12.8/5.12.8/mingw73_64/bin
export OPENSSL_PATH=/c/Program\ Files/OpenSSL-Win64

$MINGW_BIN/windeployqt.exe --release $RELEASE_PATH

# Copy missing dependencies
cp $MINGW_BIN/libgcc_s_seh-1.dll $RELEASE_PATH
#cp $MINGW_BIN/libgcc_s_sjlj-1.dll $RELEASE_PATH
cp $MINGW_BIN/libstdc++-6.dll $RELEASE_PATH
cp $MINGW_BIN/libwinpthread-1.dll $RELEASE_PATH


cp "$OPENSSL_PATH/libcrypto-1_1-x64.dll" $RELEASE_PATH
cp "$OPENSSL_PATH/libssl-1_1-x64.dll" $RELEASE_PATH
