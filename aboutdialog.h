#pragma once

#include "userhelper.h"

#include <QDialog>

namespace Ui {
class AboutDialog;
}

class AboutDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AboutDialog(QWidget *parent = nullptr);
    ~AboutDialog();

private slots:
    void getUserCallback(const User &u);

private:
    void onGotUserId(int userID);

    // Class members
    Ui::AboutDialog *ui;
    UserHelper mUserHelper;
};

