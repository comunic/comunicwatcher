#include "apirequest.h"
#include "notificationshelper.h"

NotificationsHelper::NotificationsHelper(QObject *parent) : QObject(parent)
{

}

void NotificationsHelper::getNewNotificationsNumbers()
{
    APIRequest *req = new APIRequest("notifications/count_all_news");
    req->exec();
    connect(req, &APIRequest::done, this, &NotificationsHelper::getNotificationsNumberCallback);

}

void NotificationsHelper::getNotificationsNumberCallback(APIResponse response)
{
    if(response.isError()) {
        qDebug() << "Could not get the number of unread conversations!";
        return;
    }

    auto obj = response.getObject();
    NotificationsNumber n;
    n.setNewNotifs(obj.value("notifications").toInt());
    n.setUnreadConversations(obj.value("conversations").toInt());

    emit onNewNumber(n);
}
