/**
 * Login helper
 *
 * @author Pierre Hubert
 */

#pragma once

#include <functional>

#include <QObject>

enum LoginResult {
    SUCCESS,
    BAD_PASSWORD,
    TOO_MANY_REQUEST,
    ERROR
};

typedef std::function<void (LoginResult)> vLoginCallback;

typedef std::function<void (int)> vGetUserIDCallback;

class AccountHelper
{
public:
    /**
     * Attempt to login user
     *
     * @param email User email address
     * @param password User password
     */
    static void LoginUser(const QString &email, const QString &password, const vLoginCallback cb);

    /**
     * Check out whether a user is currently signed in or not
     */
    static bool SignedIn();

    /**
     * Retrieve user login token
     */
    static QString GetLoginToken();

    /**
     * Destroy the login token of a user
     */
    static void RemoveLoginToken();

    /**
     * Get current user ID
     */
    static void GetUserID(const vGetUserIDCallback cb);
};

