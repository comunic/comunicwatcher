/**
 * This class contains the response to an API request
 *
 * @author Pierre Hubert
 */

#include <QByteArray>
#include <QJsonObject>

#pragma once

class APIResponse
{
public:
    APIResponse(int mCode, QByteArray content);

    int getCode() const;

    QByteArray getContent() const;

    bool isError();

    QJsonObject getObject() const;

private:
    int mCode;
    QByteArray mContent;
};
