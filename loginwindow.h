/**
 * Login window
 *
 * @author Pierre Hubert
 */

#pragma once

#include "apiresponse.h"
#include "accounthelper.h"

#include <QDialog>
#include <QMouseEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class LoginWindow; }
QT_END_NAMESPACE

class LoginWindow : public QDialog
{
    Q_OBJECT

public:
    LoginWindow(QWidget *parent = nullptr);
    ~LoginWindow();

protected:
    void mousePressEvent(QMouseEvent *evt);
    void mouseMoveEvent(QMouseEvent *evt);

private slots:
    /**
     * Submit login form
     */
    void submitForm();

    void onResponse(LoginResult res);

    void on_closeButton_clicked();

    void on_submitButton_clicked();

private:
    void setLoading(bool loading);

    // Class members
    Ui::LoginWindow *ui;
    QPoint mOldPos;
};
