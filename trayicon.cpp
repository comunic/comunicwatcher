#include <QApplication>
#include <QMenu>
#include <QPixmap>
#include <QPainter>

#include "trayicon.h"

TrayIcon::TrayIcon(QObject *parent) : QObject(parent)
{
    mMenu = new QMenu;

    mUnreadNotifications = mMenu->addAction(tr("Loading..."));
    mUnreadNotifications->setDisabled(true);

    mUnreadConversations = mMenu->addAction(tr("Loading..."));
    mUnreadConversations->setDisabled(true);

    QAction *openComunicAction = mMenu->addAction(tr("Open Comunic"));
    connect(openComunicAction, &QAction::triggered, this, &TrayIcon::onOpenComunic);

    mMenu->addSeparator();

    QMenu *subMenu = mMenu->addMenu(tr("Other actions"));

    QAction *aboutAction = subMenu->addAction(tr("About this application"));
    connect(aboutAction, &QAction::triggered, this, &TrayIcon::showAboutDialog);

    QAction *signOutAction = subMenu->addAction(tr("Sign out"));
    connect(signOutAction, &QAction::triggered, this, &TrayIcon::askForSignOut);

    mMenu->addSeparator();

    QAction *closeAction = mMenu->addAction(tr("Quit"));
    connect(closeAction, &QAction::triggered, this, &TrayIcon::onQuit);

    mTrayIcon.setIcon(QIcon(":/logo_large.png"));
    mTrayIcon.setContextMenu(mMenu);
    mTrayIcon.show();

    connect(&mTrayIcon, &QSystemTrayIcon::messageClicked, this, &TrayIcon::onOpenComunic);
}

TrayIcon::~TrayIcon()
{
    mMenu->deleteLater();
}

void TrayIcon::onNewNumber(const NotificationsNumber &number)
{
    if(number.sum() > mOldNumber) {
        showNotification(number);
    }

    mOldNumber = number.sum();
    refreshIcon();

    mUnreadNotifications->setText(QString(number.newNotifs() < 2 ?
                                              tr("%1 notification") :
                                              tr("%1 notifications")
    ).arg(number.newNotifs()));


    mUnreadConversations->setText(QString(number.unreadConversations() < 2 ?
                                              tr("%1 unread conversation") :
                                              tr("%1 unread conversations")
    ).arg(number.unreadConversations()));
}

void TrayIcon::onQuit()
{
    QApplication::exit();
}


void TrayIcon::showNotification(const NotificationsNumber &n)
{
    QString msg;

    if(n.newNotifs() == 1)
        msg += tr("1 new notification");
    else if(n.newNotifs() > 1)
        msg += tr("%1 new notifications").arg(n.newNotifs());


    if(n.unreadConversations() > 0) {
        if(!msg.isEmpty())
            msg += tr(" and ");

        if(n.unreadConversations() == 1)
            msg += tr("1 unread conversation");
        else if(n.unreadConversations() > 1)
            msg += tr("%1 unread conversations").arg(n.unreadConversations());
    }

    msg = tr("You have %1.").arg(msg);

    mTrayIcon.showMessage(tr("Comunic"), msg, QIcon(":/logo_large.png"));
}

void TrayIcon::refreshIcon()
{
    if(mOldNumber == 0) {
        mTrayIcon.setIcon(QIcon(":/logo_large.png"));
        return;
    }


    QPixmap bm = QPixmap::fromImage(QImage(":/logo_large.png"), Qt::ColorOnly);

    QPainter engine(&bm);
    QColor red(255, 0, 0, 255);
    QRect r(QPoint(bm.width()/3, 0), QPoint(bm.width(), bm.height() - bm.height()/3));
    engine.fillRect(r, red);

    engine.setPen(QColor(255,255,255,255));

    QFont f = engine.font();
    f.setFamily("Arial");
    f.setPointSize(bm.width()/(mOldNumber < 10 ? 1.8 : 2.3));
    f.setBold(true);
    engine.setFont(f);

    engine.drawText(r, Qt::AlignCenter, QString::number(mOldNumber));

    mTrayIcon.setIcon(QIcon(bm));
}
